# Installation
1. Install live-server from npm package:
```
npm install -g live-server
```
2. Open a terminal, then cd to the folder:
```
cd cctv-frontend
```
3. Run live-server from terminal:
```
live-server
```