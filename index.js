const ALERT_SERVER = 'http://localhost:3000';
const STREAM_SERVER = 'ws://127.0.0.1:9999';
const content = document.querySelector('#cam_content');
const canvas = document.querySelector('#cam_canvas');

let source = '';

if (!!window.EventSource) {
  source = new EventSource(`${ALERT_SERVER}/api/alert`);
} else {
  throw new Error('No EventSource API support');
}

source.addEventListener(
  'alert',
  function(e) {
    const data = e.data;
    window.alert(data);
    content.innerHTML = data;

    const websocket = new WebSocket(STREAM_SERVER);
    const player = new jsmpeg(websocket, {
      canvas: canvas,
      autoplay: true,
      loop: true
    });
  },
  false
);

source.addEventListener(
  'open',
  function(e) {
    // Connection was opened.
  },
  false
);

source.addEventListener(
  'error',
  function(e) {
    if (e.readyState == EventSource.CLOSED) {
      // Connection was closed.
    }
  },
  false
);
